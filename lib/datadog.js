'use strict';

const dogapi = require('dogapi'),
    config = require('../config');

let initialized = false;

const tags = {
    env: config.NODE_ENV
};

function addTags(newTags) {
    Object.assign(tags, newTags);
}

function sendMetric(metric, points) {
    if (!initialized) {
        initialize();
    }
    return dogapi.metric.send(metric, points, { tags });
}

function initialize(apiKey=config.DD_API_KEY, appKey=config.DD_APP_KEY, additionalTags={}) {
    dogapi.initialize({
        api_key: apiKey,
        app_key: appKey
    });

    Object.assign(tags, additionalTags);

    initialized = true;
}

module.exports = {
    sendMetric,
    addTags,
    initialize
};