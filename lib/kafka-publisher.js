'use strict';

const NodeRDKafka = require('node-rdkafka'),
    logger = require('./logger');

class KafkaPublisher {
  constructor (hosts, user, password) {
    this.producer = new NodeRDKafka.HighLevelProducer({
      'metadata.broker.list': hosts, //string of comma-separated host names
      'security.protocol': 'SASL_SSL',
      'sasl.mechanisms': 'PLAIN',
      'sasl.username': user, //Kafka cluster API key
      'sasl.password': password, //Kafka cluster API secret
    });


    this.producer.setKeySerializer(KafkaPublisher._bufferString);
    this.producer.setValueSerializer(KafkaPublisher._bufferString);

    const promiseReturn = this;
    return new Promise((resolve, reject) => {
      this.producer.connect({}, (err) => {
        if (err) {
          return reject(err);
        }
        logger.debug('Kafka Publisher is connected');
        return resolve(promiseReturn);
      });
    });
  }

  static _bufferString(str) {
    return Buffer.from(str);
  }

  async disconnect() {
    return this.producer.disconnect();
  }

  async publishOne(topic, key, message, timeoutInMilliseconds, partition=-1) {
    const payload = [{ key, message }];
    return await this.publishMany(topic, payload, timeoutInMilliseconds, partition);
  }

  // payload is [{ key: key1, message: message1 }, ... ]
  async publishMany(topic, payload, timeoutInMilliseconds=1500, partition=-1) {
    const messageAcks = [];
    for (let i = 0; i < payload.length; ++i) {
      const item = payload[i];
      messageAcks.push(new Promise((resolve, reject) => {
        this.producer.produce(topic, partition, item.message, item.key, Date.now(), (err) => {
          if (err) {
            return reject(err);
          }
          return resolve();
        });
      }));
    }


    return new Promise((resolve, reject) => {
      this.producer.flush(timeoutInMilliseconds,(err) => {
        if (err) {
          return reject(err);
        }
        Promise.all(messageAcks).then(() => {
          resolve();
        }).catch(reject);
      });
    })
  }
}

module.exports = {
  KafkaPublisher
};
