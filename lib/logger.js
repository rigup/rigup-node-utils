'use strict';

const winston = require('winston'),
    Sentry = require('winston-transport-sentry'),
    config = require('../config');

module.exports = winston.createLogger({
    level: config.LOG_LEVEL,
    format: winston.format.combine(
        winston.format.timestamp({
            format: 'YYYY-MM-DD HH:mm:ss'
        }),
        winston.format.json()
    ),
    transports: [
        new winston.transports.Console(),
        new Sentry({
            dsn: config.SENTRY_DSN,
            patchGlobal: true
        })
    ]
});
