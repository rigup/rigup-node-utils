'use strict';

const NodeRDKafka = require('node-rdkafka'),
    delay = require('delay'),
    logger = require('./logger');

async function defaultHandleError(err) {
    logger.error('Kafka Consumer error: ' + err);

    //exit to avoid committing offsets
    process.exit(1);
}

class KafkaAdmin {

    constructor(hosts, user, password) {
        this.adminClient = NodeRDKafka.AdminClient.create({
            'metadata.broker.list': hosts,
            'sasl.mechanisms': 'PLAIN',
            'security.protocol': 'SASL_SSL',
            'sasl.username': user, //Kafka cluster API key
            'sasl.password': password, //Kafka cluster API secret
        });
    }

    async createTopic(topicName, partitionCount, replicationFactor=3, timeout=3000) {
        return new Promise((resolve, reject) => {
            this.adminClient.createTopic({
                topic: topicName,
                num_partitions: partitionCount,
                replication_factor: replicationFactor
            }, timeout, async (err) => {
                if (err) {
                    return reject(err);
                }
                await delay(timeout);
                return resolve();
            });
        });
    }

    async deleteTopic(topicName, timeout=3000) {
        return new Promise((resolve, reject) => {
            this.adminClient.deleteTopic(topicName, timeout, async (err) => {
                if (err) {
                    return reject(err);
                }
                // this doesn't actually wait for confirmation, so sleep for that time
                await delay(timeout);
                return resolve();
            });
        });
    }
}

module.exports = {
    KafkaAdmin
};