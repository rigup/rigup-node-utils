'use strict';

const AWS = require('aws-sdk'),
    fs = require('fs');

class S3 {

    static DEFAULT_ACL = 'bucket-owner-full-control';

    constructor(accessKeyId, secretAccessKey, region) {
        this.config = new AWS.Config();
        this.config.update({
            accessKeyId,
            secretAccessKey,
            region
        });

        AWS.config = this.config;
        this.s3 = new AWS.S3();
    }

    async uploadToS3(localFilename, bucket, s3Filename, acl=S3.DEFAULT_ACL) {
        AWS.config = this.config;

        const dataStream = fs.createReadStream(localFilename);

        return await this.s3.upload({
            Bucket: bucket,
            Key: s3Filename,
            Body: dataStream,
            ACL: acl
        }).promise();
    }
}

module.exports = {
    S3
};