'use strict';

const { Pool } = require('pg'),
    logger = require('./logger'),
    pgConnectionParser = require('pg-connection-string/index');


class PostgresDB {

    // return types, for PostgresDB.query
    static ALL_ROWS = 0;        // SELECT * FROM user                   ->  [ { id: 1, name: 'Alice' }, { id: 2, name: 'Bob' }
    static LIST = 1;            // SELECT name FROM user                ->  [ 'Alice', 'Bob' ]
    static SINGLE_ROW = 2;      // SELECT * FROM user LIMIT 1           ->  { id: 1, name: 'Alice' }
    static SCALAR = 3;          // SELECT name FROM user LIMIT 1        -> 'Alice'
    static RAW = 4;             // simply returns the raw output of the node postgres library
    static NONE = 5;
    static ROWCOUNT = 6;        // SELECT * FROM user                   -> 3

    constructor(config, poolSize=1) {

        if (typeof config === 'string') {
            config = pgConnectionParser(config);
        }

        this.config = Object.assign({}, config);

        const pgLibraryPoolsizeKey = 'max';

        if (!this.config.hasOwnProperty(pgLibraryPoolsizeKey)) {
            this.config[pgLibraryPoolsizeKey] = poolSize;
        }

        this.pool = new Pool(config);
    }

    async end() {
        return await this.pool.end()
    }

    getConfigForDifferentDatabase(database) {
        const config = Object.assign( {}, this.config);
        config.database = database;
        return config;
    }

    getConnectionToDifferentDatabase(database) {
        const config = this.getConfigForDifferentDatabase(database);
        return new PostgresDB(config);
    }

    async dropConnectionsFromDatabase(databaseName, warn=true) {
        const droppedDatabaseConnections = await this.query(`
            SELECT pg_terminate_backend(pg_stat_activity.pid)
            FROM pg_stat_activity
            WHERE pg_stat_activity.datname = $1 AND pid <> pg_backend_pid()
        `, [databaseName], PostgresDB.ROWCOUNT);

        if (droppedDatabaseConnections && warn) {
            logger.warn(`Dropped ${droppedDatabaseConnections} connections from ${databaseName}`);
        }
    }

    async query(query, params, returnType=PostgresDB.ALL_ROWS) {
        const result = await this.pool.query(query, params);
        return PostgresDB._formatResult(result, returnType);
    }

    static _formatResult(result, returnType) {
        switch(returnType) {
            case PostgresDB.NONE:
                return;
            case PostgresDB.ALL_ROWS:
                return result.rows;
            case PostgresDB.LIST:
                const listKey = result.fields[0].name;
                return result.rows.map(row => row[listKey]);
            case PostgresDB.SINGLE_ROW:
                if (!result.rows.length) {
                    return null;
                }
                return result.rows[0];
            case PostgresDB.SCALAR:
                if (!result.rows.length) {
                    return undefined;
                }
                const scalarKey = result.fields[0].name;
                return result.rows[0][scalarKey];
            case PostgresDB.RAW:
                return result;
            case PostgresDB.ROWCOUNT:
                return result.rowCount;
            default:
                throw Error('Invalid return type passed to Postgres lib');
        }
    }

    async transaction(queries) {
        const client = await this.pool.connect();
        try {
            await client.query('BEGIN');
            for (let i = 0; i < queries.length; ++i) {
                await client.query(queries[i]);
            }
            await client.query('COMMIT');
        } catch (e) {
            await client.query('ROLLBACK');
            throw e;
        } finally {
            client.release();
        }
    }

    async ensureExtensionsArePublic() {
        await this.query(`
            DO
            $$
            DECLARE
                row record;
            BEGIN
                FOR row IN SELECT extname FROM pg_extension WHERE extname <> 'plpgsql'
                LOOP
                    EXECUTE 'ALTER EXTENSION ' || quote_ident(row.extname) || ' SET SCHEMA public';
                END LOOP;
            END;
            $$;`
        );
    }
}

module.exports = {
    PostgresDB
};