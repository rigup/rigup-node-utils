'use strict';

function randomString(length, charset='abcdefghijklmnopqrstuvwxyz') {
    const chars = [];
    for (let i = 0; i < length; ++i) {
        chars.push(charset[Math.floor(Math.random() * charset.length)]);
    }
    return chars.join('');
}

module.exports = randomString;