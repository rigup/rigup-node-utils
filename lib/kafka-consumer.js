'use strict';

const NodeRDKafka = require('node-rdkafka'),
    logger = require('./logger');

 function defaultHandleError(err) {
    logger.error('Kafka Consumer error: ' + err);

    //exit to avoid committing offsets
    process.exit(1);
}

class KafkaConsumer {

    constructor(hosts, user, password, groupId, topic, messageHandleFunction, errorHandleFunction=defaultHandleError) {
        this.consumer = new NodeRDKafka.KafkaConsumer({
            'metadata.broker.list': hosts,
            'sasl.mechanisms': 'PLAIN',
            'security.protocol': 'SASL_SSL',
            'sasl.username': user, //Kafka cluster API key
            'sasl.password': password, //Kafka cluster API secret
            'group.id': groupId,
            'enable.auto.commit': true
        }, {
            'auto.offset.reset': 'earliest'
        });

        const returnValue = this;
        return new Promise((resolve, reject) => {
            try {
                this.consumer.on('ready', () => {
                    this.consumer.subscribe([topic]);
                    this.consumer.consume();
                    resolve(returnValue);
                })
                .on('data', messageHandleFunction)
                .on('event.error', errorHandleFunction);

                this.consumer.connect();
            } catch (e) {
                reject(e);
            }
        });
    }

    disconnect() {
        return this.consumer.disconnect();
    }

}

module.exports = {
    KafkaConsumer
};