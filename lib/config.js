'use strict';

/*
    Should be called at entry point of project
 */
function init(config) {
    Object.assign(configSettings, config)
}

/*
    Allows users to reference other environment variables to enable multi-env stuff by appending _{ENV} to variables
    For example, if ANALYTICS_DB=STAGING, then if it exists, ANALYTICS_DB_STAGING will be used as ANALYTICS_DB
 */
function defineMultiEnvironmentVariable(varName, commandLineArgs) {

    if (commandLineArgs[varName]) {
        varName += '_' + commandLineArgs[varName];
    }

    return process.env[varName];
}

const configSettings = {
    init,
    defineMultiEnvironmentVariable
};

module.exports = configSettings;