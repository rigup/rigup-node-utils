# Rigup Node Utils
#####Reusable node libraries to be shared between projects

###Modules
* A list of modules can be found in index.js

###Testing
* `cp test/local.env .env`
* `docker-compose up -d`
* `npm test`

###Deploying
* Update the version!!!
    * Always, always update the version in package.json
* Push the code.