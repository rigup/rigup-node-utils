'use strict';


/*
    This is only used for running this package's tests
 */

const NODE_ENV = process.env.NODE_ENV || 'development';

/*
    Pull in local environment variables
 */
if (NODE_ENV === 'development' || NODE_ENV === 'test') {
    require('dotenv').config();
}

module.exports = {
    NODE_ENV: NODE_ENV,
    TEST_DB: process.env.TEST_DB,
    DD_API_KEY: process.env.DD_API_KEY,
    DD_APP_KEY: process.env.DD_APP_KEY,
    SENTRY_DSN: process.env.SENTRY_DSN,
    KAFKA_HOSTS: process.env.KAFKA_HOSTS,
    KAFKA_CLUSTER_API_KEY: process.env.KAFKA_CLUSTER_API_KEY,
    KAFKA_CLUSTER_API_SECRET: process.env.KAFKA_CLUSTER_API_SECRET,
    LOG_LEVEL: process.env.LOG_LEVEL || 'info'
};
