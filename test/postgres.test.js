const chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    { PostgresDB } = require('../lib/postgres'),
    { TEST_DB } = require('../config');

const expect = chai.expect;
chai.use(chaiAsPromised);

let pool;

describe('postgres', () => {
    const testDbName = 'pglib_test_temp_db';
    let testDb, testDbConnector;

    before(async () => {
        testDbConnector = new PostgresDB(TEST_DB, pool);
    });

    after(async () => {
        await testDbConnector.end();
    });

    beforeEach(async () => {
        await testDbConnector.query(`DROP DATABASE IF EXISTS ${testDbName}`);
        await testDbConnector.query(`CREATE DATABASE ${testDbName}`);
        testDb = testDbConnector.getConnectionToDifferentDatabase(testDbName);
        await testDb.query('CREATE TABLE people (id serial, first varchar(255))');
        await testDb.query(`
                INSERT INTO people
                (id, first)
                VALUES
                (2, $2),
                (3, $3),
                (1, $1)
            `, ['Alice', 'Bob', 'Charlie']);
    });

    afterEach(async () => {
        await testDb.end();
        await testDbConnector.query(`DROP DATABASE IF EXISTS ${testDbName}`);
    });

    describe('Return Type Transformations', () => {

        it('Defaults to ALL_ROWS, which returns sensible data', async () => {
            const result = await testDb.query('SELECT * FROM people ORDER BY id DESC', []);
            expect(result).to.include.deep.ordered.members([
                { id: 3, first: 'Charlie' },
                { id: 2, first: 'Bob' },
                { id: 1, first: 'Alice' }

            ]);
        });

        it('ALL_ROWS returns empty array when no results are found', async () => {
            const result = await testDb.query('SELECT * FROM people WHERE false', [], PostgresDB.ALL_ROWS);
            expect(result).to.be.an('array').that.is.empty;
        });

        it('LIST works', async () => {
            const result = await testDb.query('SELECT first FROM people ORDER BY id ASC', [], PostgresDB.LIST);
            expect(result).to.include.ordered.members(['Alice', 'Bob', 'Charlie']);
        });

        it('LIST returns empty array when no results are found', async () => {
            const result = await testDb.query('SELECT first FROM people WHERE false', [], PostgresDB.LIST);
            expect(result).to.be.an('array').that.is.empty;
        });

        it('SINGLE_ROW works', async () => {
            const result = await testDb.query('SELECT * FROM people WHERE first = $1', ['Alice'], PostgresDB.SINGLE_ROW);
            expect(result).to.deep.equal({ id: 1, first: 'Alice' });
        });

        it('SINGLE_ROW returns null if no results are found', async () => {
            const result = await testDb.query('SELECT * FROM people WHERE false', [], PostgresDB.SINGLE_ROW);
            expect(result).to.be.null;
        });

        it('SCALAR works', async () => {
            const result = await testDb.query('SELECT first FROM people WHERE id = $1', [1], PostgresDB.SCALAR);
            expect(result).to.equal('Alice');
        });

        it('SCALAR is undefined if there are no rows', async () => {
            const result = await testDb.query('SELECT first FROM people WHERE false', [], PostgresDB.SCALAR);
            expect(result).to.be.undefined;
        });

        it('ROWCOUNT should work', async () => {
            const result = await testDb.query('UPDATE people SET first = $1 WHERE first <> $2', ['Test', 'Alice'], PostgresDB.ROWCOUNT);
            expect(result).to.equal(2);
        });

        it('RAW should return a node-pg object', async () => {
            const result = await testDb.query('SELECT * FROM people', [], PostgresDB.RAW);
            expect(result).to.contain.keys(['rows', 'fields', 'rowCount', 'command']);
        });

        it('Should throw an error with unsupported return type', async () => {
            const promise = testDb.query('SELECT * FROM people', [], 'BAD_VALUE');
            await expect(promise).to.be.rejected;
        });

        it('NONE should return nothing', async () => {
            const result = await testDb.query('UPDATE people SET first = null', [], PostgresDB.NONE);
            expect(result).to.be.undefined;
        });
    });

    describe('Transactions', () => {

        it('Should error then rollback if transaction fails', async () => {
            const promise = testDb.transaction([
                `UPDATE people SET first = 'new-name' WHERE id = 1`,
                `this query is nonsense`
            ]);
            await expect(promise).to.be.rejected;

            const result = await testDb.query('SELECT first FROM people WHERE id = $1', [1], PostgresDB.SCALAR);

            expect(result).to.equal('Alice');
        });

        it('Should execute multiple queries', async () => {
            await testDb.transaction([
                `UPDATE people SET first = 'new-name' WHERE id = 1`,
                `UPDATE people SET first = 'new-name' WHERE id = 2`
            ]);

            const result = await testDb.query('SELECT first FROM people WHERE id = $1 OR id = $2', [1, 2], PostgresDB.LIST);

            expect(result).to.contain.ordered.members(['new-name', 'new-name']);
        });

    });
});