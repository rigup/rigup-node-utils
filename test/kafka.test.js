const chai = require('chai'),
    chaiAsPromised = require('chai-as-promised'),
    delay = require('delay'),
    { KafkaAdmin, KafkaConsumer, KafkaPublisher, logger } = require('../index'),
    { KAFKA_HOSTS, KAFKA_CLUSTER_API_SECRET, KAFKA_CLUSTER_API_KEY } = require('../config');

const expect = chai.expect;
chai.use(chaiAsPromised);

let pool;

describe('kafka', () => {

    let adminClient;
    const testTopic = 'testing-topic',
        testGroupName = 'kafka-test-group-name';

    before(async function() {
        this.timeout(10000);
        adminClient = new KafkaAdmin(KAFKA_HOSTS, KAFKA_CLUSTER_API_KEY, KAFKA_CLUSTER_API_SECRET);

        // ensure test topic doesn't already exist
        try {
            await adminClient.deleteTopic(testTopic);
        } catch (e) {
            const UNKNOWN_TOPIC_ERROR_CODE = 3;
            if (e.code && e.code === UNKNOWN_TOPIC_ERROR_CODE) {
                // unknown topic errors can be ignored
                return;
            }
            throw e;
        }

        // delete topic does not correctly halt until deletion is complete
        await delay(5000);

    });

    beforeEach(async function() {
        this.timeout(10000); //usually takes ~6 seconds
        await adminClient.createTopic(testTopic, 3);
    });

    afterEach(async function() {
        this.timeout(10000); //usually takes ~6 seconds
        await adminClient.deleteTopic(testTopic);
    });

    describe('Test Basic Kafka Functionality', () => {

        it('Can send and receive a message', async () => {

            const publisher = await new KafkaPublisher(KAFKA_HOSTS, KAFKA_CLUSTER_API_KEY, KAFKA_CLUSTER_API_SECRET);

            const messageId = 'theMessageId';
            const message = JSON.stringify({
                test: 'yes'
            });

            await publisher.publishOne(testTopic, messageId, message);
            await publisher.disconnect();

            let consumer = null;
            const waitForMessage = new Promise(async (resolve, reject) => {
                consumer = await new KafkaConsumer(KAFKA_HOSTS, KAFKA_CLUSTER_API_KEY, KAFKA_CLUSTER_API_SECRET, testGroupName, testTopic, async (data) => {
                    resolve(data.value.toString());
                });
            });

            const receivedMessage = await waitForMessage;

            expect(receivedMessage).to.deep.equal(message);

            consumer.disconnect();
        }).timeout(10000);
    });
});
